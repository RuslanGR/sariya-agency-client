import axios from "axios";

export const base = "http://127.0.0.1:8000";

export default {
  auth: {
    login: credentials =>
      axios.post(`${base}/api/token/`, credentials).then(res => res.data),
    signup: credentials => {
      console.log(credentials);
      return axios
        .post(`${base}/api/signup/`, { ...credentials })
        .then(response => response.data);
    }
  },
  housing: {
    getAll: () =>
      axios.get(`${base}/api/agency/housing/`).then(res => res.data),
    add: (userId, data) =>
      axios
        .post(`${base}/api/agency/order/${userId}/`, {data})
        .then(res => res.data),
    getLiked: userId =>
      axios.get(`${base}/api/agency/order/${userId}/`).then(res => res.data)
  }
};
