import React, { Component } from "react";
import { Route, Switch } from "react-router";

import Navbar from "./components/layouts/Navbar";
import NotFoundPage from "./components/pages/NotFoundPage";
import SignupPage from "./components/pages/SignupPage";
import LoginPage from "./components/pages/LoginPage";
import LikedPage from "./components/pages/LikedPage";
import jwtDecode from "jwt-decode";
import setAuthHeaders from "./setAuthHeaders";
import IndexPage from "./components/pages/IndexPage";
import api from "./api";

class App extends Component {
  state = {
    userId: null,
    authorized: false,
    errors: null
  };

  componentWillMount() {
    if (localStorage.AgencyToken) {
      setAuthHeaders(localStorage.AgencyToken);
      const payload = jwtDecode(localStorage.AgencyToken);
      this.setState({
        authorized: true,
        userId: payload.user_id
      });
    } else {
      this.setState({ authorized: false });
    }
  }

  signUp = credentials => {
    api.auth
      .signup(credentials)
      .then(res => this.login(credentials.email, credentials.password));
    this.props.history.push("/");
  };

  logout = () => {
    this.setState({ authorized: false, userId: null });
    setAuthHeaders();
    localStorage.removeItem("AgencyToken");
  };

  login = (email, password) => {
    this.setState({ errors: null });
    api.auth
      .login({ email, password })
      .then(res => {
        const payload = jwtDecode(res.access);
        localStorage.AgencyToken = res.access;
        setAuthHeaders(res.access);
        this.setState({
          authorized: true,
          errors: "",
          userId: payload.user_id
        });
        this.props.history.push("/");
      })
      .catch(err =>
        this.setState({ errors: "Авторизационные данные не верны" })
      );
  };

  render() {
    const { authorized, userId, errors } = this.state;
    return (
      <>
        <Navbar authorized={authorized} logout={this.logout} />
        <Switch>
          <Route
            path="/"
            exact
            render={() => <IndexPage authorized={authorized} userId={userId} />}
          />
          <Route
            path="/liked"
            exact
            render={() =>
              authorized ? (
                <LikedPage authorized={authorized} userId={userId} />
              ) : (
                <NotFoundPage />
              )
            }
          />
          <Route
            path="/login"
            render={() => (
              <LoginPage serverErrors={errors} login={this.login} />
            )}
          />
          <Route
            path="/signup"
            render={() => <SignupPage signup={this.signUp} />}
          />

          <Route component={NotFoundPage} />
        </Switch>
      </>
    );
  }
}

export default App;
