import React, { Component } from "react";
import validator from "validator";
import "./LikedPage.css";

class LoginPage extends Component {
  state = {
    email: "",
    password: "",
    errors: {}
  };

  submit = event => {
    event.preventDefault();
    const { email, password } = this.state;
    const errors = this.validate(this.state);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) this.props.login(email, password);
  };

  validate = data => {
    const errors = {};
    if (!data.email) errors.email = "Login is required!";
    if (!validator.isEmail(data.email))
      errors.email = "It doesn't looks like email";
    if (!data.password) errors.password = "Password is required!";
    return errors;
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    const { email, password, errors } = this.state;

    return (
      <div className="container py-4">
        <div className="row">
          <div className="col-4 offset-4">
            <h2 className="py-4">Войти</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 offset-md-4 bg-light rounded py-5">
            {this.props.serverErrors && (
              <h6 style={{ color: "red" }}>{this.props.serverErrors}</h6>
            )}
            <form method="POST" onSubmit={this.submit}>
              <div className="form-group">
                <label htmlFor="">Логин</label>
                <input
                  tabIndex="0"
                  type="text"
                  className="form-control"
                  value={email}
                  onChange={this.onChange}
                  name="email"
                />
                {errors.email && (
                  <span style={{ color: "red" }}>{errors.email}</span>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="">Пароль</label>
                <input
                  type="password"
                  className="form-control"
                  value={password}
                  onChange={this.onChange}
                  name="password"
                />
                {errors.password && (
                  <span style={{ color: "red" }}>{errors.password}</span>
                )}
              </div>
              <button className="card-box__button">Войти</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;
