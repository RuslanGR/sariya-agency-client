import React from "react";
import api from "../../api";
import "./LikedPage.css";

class IndexPage extends React.Component {
  state = {
    housings: []
  };

  componentDidMount() {
    api.housing.getAll().then(res => this.setState({ housings: res }));
  }

  addLiked = id => {
    api.housing.add(this.props.userId, { id });
  };

  render() {
    const { housings } = this.state;
    const { authorized } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col py-4">
            <h2>Доступные предложения</h2>
          </div>
        </div>
        <div className="row">
          <div className="col">
            {housings &&
              housings.map(h => (
                <div className="card-box" key={h.id}>
                  <div className="row">
                    <div className="col-4">
                      <img
                        src={h.image}
                        className="card-box__image"
                        alt="image"
                      />
                    </div>
                    <div className="col-8">
                      <div className="card-box__header">{h.address}</div>
                      <div className="car-box__description">
                        {h.description}
                      </div>
                      <span className="py-4">{h.owner.phone_number}</span>
                      <h4>{h.price}р.</h4>
                      {authorized && (
                        <button
                          className="card-box__button"
                          onClick={() => this.addLiked(h.id)}
                        >
                          В избранное
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default IndexPage;
