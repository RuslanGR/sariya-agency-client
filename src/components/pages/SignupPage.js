import React, { Component } from "react";
import "./LikedPage.css";
import validator from "validator";

class LoginForm extends Component {
  state = {
    data: {
      password: "",
      password2: "",
      email: ""
    },
    errors: {},
    loading: false
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      delete this.state.data.password2;
      this.props.signup(this.state.data);
    }
  };

  validate = data => {
    const errors = {};
    if (!data.password) errors.password = "This field is required!";
    if (!data.password2) errors.password2 = "This field is required!";
    if (!validator.isEmail(data.email))
      errors.email = "It doesn't looks like email";
    if (data.password !== data.password2)
      errors.password = "Passwords must be similar.";
    if (!data.email) errors.email = "Username is required!";
    return errors;
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="offset-4 col-4">
            <h2 className="py-4">Регистрация</h2>
          </div>
        </div>
        <div className="row">
          <div className="offset-4 col-4">
            <form method="post" onSubmit={this.onSubmit} className="py-4">
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  onChange={this.onChange}
                  type="text"
                  name="email"
                  className="form-control"
                  value={this.state.email}
                />
                {errors.email && (
                  <span style={{ color: "red" }}>{errors.email}</span>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="password">Пароль</label>
                <input
                  onChange={this.onChange}
                  type="password"
                  name="password"
                  className="form-control"
                  value={this.state.password}
                />
                {errors.password && (
                  <span style={{ color: "red" }}>{errors.password}</span>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="password2">Подтверждение пароля</label>
                <input
                  onChange={this.onChange}
                  type="password"
                  name="password2"
                  className="form-control"
                  value={this.state.password2}
                />
                {errors.password2 && (
                  <span style={{ color: "red" }}>{errors.password2}</span>
                )}
              </div>
              <button className="card-box__button">Зарегистрироваться</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginForm;
