import React from "react";
import api, { base } from "../../api";
import "./LikedPage.css";

class LikedPage extends React.Component {
  state = {
    housings: []
  };

  componentDidMount() {
    this.getLiked();
  }

  getLiked = () => {
    api.housing.getLiked(this.props.userId).then(res => {
      console.log(res);
      this.setState({ housings: res });
    });
  };

  deleteHousing = id => {
    api.housing
      .add(this.props.userId, { id, delete: "1" })
      .then(() => this.getLiked());
  };

  render() {
    const { housings } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col py-4">
            <h2>Понравившиеся дома</h2>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="housing__container d-flex">
              {housings &&
                housings.map(item => (
                  <div className="card-box" key={item.id}>
                    <div className="row">
                      <div className="col-4">
                        <img
                          src={`${base}${item.housing.image}`}
                          className="card-box__image"
                          alt="image"
                        />
                      </div>
                      <div className="col-8">
                        <div className="card-box__header">
                          {item.housing.address}
                        </div>
                        <div className="car-box__description">
                          {item.housing.description}
                        </div>
                        <span className="py-4">
                          {item.housing.owner.phone_number}
                        </span>
                        <h4>{item.housing.price}р.</h4>
                        <button
                          className="card-box__button"
                          onClick={() => this.deleteHousing(item.housing.id)}
                        >
                          Убрать
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LikedPage;
