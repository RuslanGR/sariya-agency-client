import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = ({ authorized, logout }) => (
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="container">
      <NavLink className="primary text-uppercase navbar-brand" to="/">
        Real estate agnecy
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarText"
        aria-controls="navbarText"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <NavLink className="nav-link" to="/">
              Главная <span className="sr-only">(current)</span>
            </NavLink>
          </li>
          {authorized && (
            <li className="nav-item">
              <NavLink className="nav-link" to="/liked">
                Понравивишиеся
              </NavLink>
            </li>
          )}
        </ul>
        {!authorized ? (
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className="nav-link" to="/login">
                Войти
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/signup">
                Зарегистрироваться
              </NavLink>
            </li>
          </ul>
        ) : (
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className="nav-link" to="/" onClick={() => logout()}>
                Выйти
              </NavLink>
            </li>
          </ul>
        )}
      </div>
    </div>
  </nav>
);

export default Navigation;
